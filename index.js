/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import nhapthongtinkhachhang from './nhapthontinkhachhang';
import nhapthongtinchitietkhachhang from './nhapthongtinchitietkhachhang';
import AppNavigator from './App';
import map from './map';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Module RCTImageLoader requires',
]);

AppRegistry.registerComponent(appName, () =>AppNavigator);
