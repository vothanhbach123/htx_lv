/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  style,
  Button,
  height,
  width,
  FlatList,
  Picker,
  TextInput,
  TouchableOpacity
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import { Marker, Callout, AnimatedRegion } from 'react-native-maps';
import Icon from 'react-native-vector-icons/Fontisto'
//import ClusteredMapView from 'react-native-maps-super-cluster';
import GetLocation from 'react-native-get-location'


export default class map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      LatLng: {
        
        latitude: 10.4428,
        longitude: 106.4312
      },
      Region: {
        longitude: 105.6,
        latitude: 10.2,
        latitudeDelta: 3.22,
        longitudeDelta: 2.22,
      },

      markers: [{
        title: 'hello',
        coordinates: {
          latitude: 9.148561,
          longitude: 106.652778
        },
      },
      {
        title: 'hebachllo',
        coordinates: {
          latitude: 10.149771,
          longitude: 106.655449
        },
      }]
    }

  }
  // async componentDidMount() {
  //   return fetch('http://192.168.81.107:8080/database/FlowersList.php',{ method: "POST", body: JSON.stringify({ sodondh: "65" }) })
  //     .then((response) => response.json())
  //     .then((responseJson) => {

  //       this.setState({
  //         isLoading: false,
  //         dataSource: responseJson,
  //       }, function () {

  //       });

  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });
  // }
  onpress() {
    //alert("vo" + "\n" + "thanh");
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(location => {
        alert("" + location.latitude + "\n" + location.longitude + "\n" + location.latitudeDelta);
        // renderMarker(this.state.LatLng);
        this.setState({ LatLng: { latitude: location.latitude, longitude: location.longitude } });
      })
      .catch(error => {
        const { code, message } = error;
        console.warn(code, message);
      })
  }
  // onlick() {
  //   // this.setState({ LatLng: { latitude: 10, longitude: 105 } })
  //   data=this.state.dataSource
  //   data.map((data,index)=>{
  //     alert(""+data.sodondh);
  //     key={index}
  //   })
  // }
  // componentDidMount() {
  //   GetLocation.getCurrentPosition({
  //     enableHighAccuracy: true,
  //   })
  //     .then(location => {
  //       // alert("" + location.latitude + "\n" + location.longitude + "\n" + location.latitudeDelta);
  //       // renderMarker(this.state.LatLng);
  //       this.setState({ LatLng: { latitude: location.latitude, longitude: location.longitude } });
  //     })
  //     .catch(error => {
  //       const { code, message } = error;
  //       console.warn(code, message);
  //     })
  // }

  componentDidMount() {
    return
    this.state.Region;
  }
  onclick(text) {
      let region={
        longitude: 105.6,
        latitude: 10.2,
        latitudeDelta: 0.33332,
        longitudeDelta: 0.33333,
      }
     this.mapView.animateToRegion(region,1000);
  }


  render() {

    return (
      <View style={styles.container}>
        <View
          style={(styles.borderRadius)}
        >
          <TextInput></TextInput>
        </View>
        <View style={styles.picker}>
          <TouchableOpacity
            style={styles.thanhpho_picker}
            onPress={()=>this.onclick()}
          >
            <Text> Touch Here </Text>
          </TouchableOpacity>
          
          <Picker
            selectedValue={this.state.language}
            style={styles.quan_picker}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({ language: itemValue })
            }>

          </Picker>
        </View>
        <MapView
          ref={ref => { this.mapView= ref; }}
          provider={PROVIDER_GOOGLE}
          initialRegion={
            this.state.Region
          }
          showsUserLocation={true}
          style={styles.map}

        >

          {/* <Marker
            coordinate={
              LatLng = {
                latitude: 10.2,
                longitude: 105.6
              }
            }
            // title={"sdfsd"}
            onPress={() => this.onclick()}
          > */}
          {/* <Callout style={styles.plainView}> */}
          {/* <View>
              <Text>{this.state.Region.latitudeDelta}</Text>
            </View> */}
          {/* </Callout> */}


          {/* </Marker> */}
          {this.state.markers.map((marker, index) => (
            <MapView.Marker
              key={index}
              coordinate={marker.coordinates}
              title={marker.title}
              onPress={() => this.onclick(marker.title)}
            >
              <View style={styles.plainView}>
                <Icon
                  name='map-marker'
                  //type='MaterialCommunityIcons'
                  color='green'
                  size={10}
                >
                </Icon>
                <Text>
                  asdasd
                  </Text>
              </View>
            </MapView.Marker>
          ))}

        </MapView>
      </View>
    );
  }
}

function renderMarker({ location }) {
  return (
    <MapView.Marker
      coordinate={location}
    >
      <MapView.Callout>
        <Text>BiG BiG Callout</Text>
      </MapView.Callout>
    </MapView.Marker>
  );
}
const styles = StyleSheet.create({
  customView: {
    width: 100,
    height: 500,
  },
  plainView: {
    alignItems: 'center'
  },
  container: {
    // ...StyleSheet.absoluteFillObject,
    // justifyContent: 'flex-end',
    alignItems: 'baseline',
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'gray'
  },
  eventList: {
    //position: 'absolute',
    // top: height / 2,
    // left: 0,
    // right: 0,
    // bottom: 0,
    //justifyContent: 'flex-end',
    // height: 500,
    //...StyleSheet.absoluteFill,
    top: 10,
  },
  map: {

    // right: 0,
    // bottom: height / 2,
    ...StyleSheet.absoluteFillObject,
    top: 100,
  },
  borderRadius: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: 'white',
    width: 250,
    height: 35,
    top: 10,
    left: 15
  },
  picker: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: 'white',
    width: 290,
    height: 35,
    top: 15,
    left: 15,

  },
  thanhpho_picker: {
    position: 'absolute',
    left: 15,
    height: 20,
    width: 120,
    top: 7,
  },
  quan_picker: {
    position: 'absolute',
    left: 100,
    height: 20,
    width: 120,
    top: 7,
  }

});