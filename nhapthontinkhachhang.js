/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  style,
  Button,
  height,
  width,
  FlatList,
  Picker,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import { CustomPicker } from 'react-native-custom-picker'
import { ScrollView } from 'react-native-gesture-handler';
export default class nhapthongtinkhachhang extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loaihinh: "DN",
      nganhang: "ACB",
      gioitinh: "nam",
      chucvu: "GD",
      tenhtx: "",
      masothue: "",
      tk: "",
      dientich: "",
      quoctich: "",
      dantoc: "",
      sohopdong: "",
      loaihinh_border: "gray",
      nganhang_border: "gray",
      gioitinh_border: "gray",
      chucvu_border: "gray",
      tenhtx_border: "gray",
      masothue_border: "gray",
      tk_border: "gray",
      dientich_border: "gray",
      quoctich_border: "gray",
      dantoc_border: "gray",
      sohopdong_border: "gray",
      ndd_border: "gray",
      isLoading: true,
      thongtinchitiet: {
        hoten: "",
        ngaysinh: "",
        diachivp: "",
        noio: "",
        sdt: "",
        email: "",
        zalo: "",
        cmnd: "",
        ngaycap: "",
        noicap: "",
      }
    },
      this.color_ = "gray"
    this.ten = "#"
  }

  pdate() {
    var thongtinchitiet = this.props.navigation.getParam("thongtin", "");
    if (thongtinchitiet.hoten == null || thongtinchitiet.hoten == "") {
      this.ten = ""
    } else {
      this.ten = "bach"
    }

  }
  adddata(text) {
    var trangthai = 0;
    var thongtinchitiet = this.props.navigation.getParam("thongtin", "");
    var makh = this.state.loaihinh + thongtinchitiet.cmnd;
    var ndd = thongtinchitiet.hoten;
    if (this.ten == "" || this.ten == "#") {
      this.color_ = "red";
      trangthai = 1;
    }
    if (this.state.loaihinh.length == "") {
      this.setState({ loaihinh_border: "red" });
      trangthai = 1;
    }
    if (this.state.tenhtx.length == "") {
      this.setState({ tenhtx_border: "red" });
      trangthai = 1;
    }
    if (this.state.masothue.length == "") {
      this.setState({ masothue_border: "red" });
      trangthai = 1;
    }
    if (this.state.tk.length == "") {
      this.setState({ tk_border: "red" });
      trangthai = 1;
    }
    if (this.state.nganhang.length == "") {
      this.setState({ nganhang_border: "red" });
      trangthai = 1;
    }
    if (this.state.chucvu.length == "") {
      this.setState({ chucvu_border: "red" });
      trangthai = 1;
    }
    if (this.state.dientich.length == "") {
      this.setState({ dientich_border: "red" });
      trangthai = 1;
    }
    if (this.state.gioitinh.length == "") {
      this.setState({ gioitinh_border: "red" });
      trangthai = 1;
    }
    if (this.state.quoctich.length == "") {
      this.setState({ quoctich_border: "red" });
      trangthai = 1;
    }
    if (this.state.dantoc.length == "") {
      this.setState({ dantoc_border: "red" });
      trangthai = 1;
    }
    if (this.state.sohopdong.length == "") {
      this.setState({ sohopdong_border: "red" });
      trangthai = 1;
    }
    if (trangthai == 1) {
      return;
    }
    else {
      return fetch('https://vothanhbach1997.000webhostapp.com/insert.php', {
        method: "POST",
        body: JSON.stringify({
          makh: makh,
          ndd: ndd,
          tenhtx: this.state.tenhtx,
          mst: this.state.masothue,
          tk: this.state.tk,
          motai: this.state.nganhang,
          chucvu: this.state.chucvu == "GD" ? "Giám đốc" : "Chủ hộ",
          dientich: this.state.dientich,
          gioitinh: this.state.gioitinh == "nam" ? "Nam" : "Nữ",
          quoctich: this.state.quoctich,
          dantoc: this.state.dantoc,
          hds: this.state.sohopdong,
          loaihinh: this.state.loaihinh == "TC" ? "Tổ chức" : "Cá nhân",

          hoten: thongtinchitiet.hoten,
          ngaysinh: thongtinchitiet.ngaysinh,
          diachivp: thongtinchitiet.diachivp,
          noio: thongtinchitiet.noio,
          sdt: thongtinchitiet.sdt,
          email: thongtinchitiet.email,
          zalo: thongtinchitiet.zalo,
          cmnd: thongtinchitiet.cmnd,
          ngaycap: thongtinchitiet.ngaycap,
          noicap: thongtinchitiet.noicap,
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function () {

          });
          data = [this.state.dataSource]
          data.map((data, index) => {
            Alert.alert(
              'Thông báo',
              data.text,
              [
                {
                  text: 'ok',
                  onPress: () => { return true; },
                  style: 'default',
                },
              ],
              { cancelable: false },
            )
            key = { index }
          })
        })
        .catch((error) => {
          Alert.alert(
            'Thông báo',
            'Có 1 vài thông tin không đúng',
            [
              {
                text: 'ok',
                onPress: () => { return true; },
                style: 'default',
              },
            ],
            { cancelable: false },
          )
        });
    }
  }
  changeLoaiHinh() {
    setTimeout(() => {
      var loaihinh = this.state.loaihinh
      if (loaihinh == "CN") {
        this.setState({ chucvu: "CH" })
      } else {
        this.setState({ chucvu: "GD" })
      }
    }, 1);
  }
  changeChucVu() {
    setTimeout(() => {
      var chucvu = this.state.chucvu
      if (chucvu == "CH") {
        this.setState({ loaihinh: "CN" })
      } else {
        this.setState({ loaihinh: "DN" })
      }
    }, 1);
  }

  render() {
    var thongtinchitiet = this.props.navigation.getParam("thongtin", "");
    if (this.ten != "#") {
      if (this.ten.length != 1) {
        this.pdate()
        if (this.ten.length == 0) {
          this.color_ = "red";
        }
        if (this.ten.length != 0) {
          this.color_ = "gray";
        }
      } else {
        if (this.ten.length == 0) {
          this.color_ = "red";
        }
        this.pdate()
      }

    }
    const options = ['One', 'Two', 'Three', 'Four', 'Five']
    return (
      <ScrollView style={styles.container3}>
        <View style={styles.container3}>
          <View style={styles.header}>
            <Text style={styles.text}>
              Nhập Thông Tin Khách Hàng
            </Text>
          </View>
          <View style={styles.container}>
            <View style={styles.container1}>
              <Text style={{ top: 10, left: 5 }}>
                Loại hình:
              </Text>
              <Text style={{ top: 20, left: 5 }}>
                Tên THT/HTX/DN:
              </Text>
              <Text style={{ top: 30, left: 5 }}>
                Người đại diện:
              </Text>
              <Text style={{ top: 55, left: 5 }}>
                Mã số thuế:
              </Text>
              <Text style={{ top: 75, left: 5 }}>
                Tài khoảng ngân hàng:
              </Text>
              <Text style={{ top: 105, left: 5 }}>
                Mở tại:
              </Text>
              <Text style={{ top: 130, left: 5 }}>
                Chức vụ:
              </Text>
              <Text style={{ top: 150, left: 5 }}>
                Diện tích:
              </Text>
              <Text style={{ top: 185, left: 5 }}>
                Giới tính:
              </Text>
              <Text style={{ top: 200, left: 5 }}>
                Quốc tịch:
              </Text>
              <Text style={{ top: 230, left: 5 }}>
                Dân tộc:
              </Text>
              <Text style={{ top: 255, left: 5 }}>
                Ký hợp đồng số:
              </Text>

            </View>
            <View style={styles.container2}>

              <CustomPicker
                options={options}
                modalStyle = {{width:150, justifyContent:"center"}}
                onValueChange={value => {
                  Alert.alert('Selected Item', value || 'No item were selected!')
                }}
              />
              <TextInput
                style={[styles.textInput1, { borderColor: this.state.tenhtx_border }]}
                onChangeText={(text) => { this.setState({ tenhtx: text }), this.setState({ tenhtx_border: "gray" }) }}
              />
              <View
                style={[styles.textInput2, { borderColor: this.color_ }]}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("nhapthongtinchitietkhachhang", { thongtin: thongtinchitiet, loaihinh: this.state.loaihinh, callback: this.pdate() })}
                >
                  <Text style={(styles.ten_NDD)}>{thongtinchitiet.hoten}</Text>
                </TouchableOpacity>
              </View>
              <TextInput
                style={[styles.textInput3, { borderColor: this.state.masothue_border }]}
                placeholder="1234567890"
                onChangeText={(text) => { this.setState({ masothue: text }), this.setState({ masothue_border: "gray" }) }}
                keyboardType='numeric'
                maxLength={10}
              />
              <TextInput
                style={[styles.textInput4, { borderColor: this.state.tk_border }]}
                placeholder="00114552455476"
                onChangeText={(text) => { this.setState({ tk: text }), this.setState({ tk_border: "gray" }) }}
                keyboardType='numeric'
                maxLength={16}
              />
              <Picker
                style={styles.textInput5}
                selectedValue={this.state.nganhang}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ nganhang: itemValue })
                }
              >
                <Picker.Item label="Ngân hàng ACB" value="Ngân hàng ACB" />
                <Picker.Item label="Ngân hàng TPBank" value="Ngân hàng TPBank" />
                <Picker.Item label="Ngân hàng KienLong" value="Ngân hàng KienLong" />
                <Picker.Item label="Ngân hàng VPBank" value="Ngân hàng VPBank" />
                <Picker.Item label="Ngân hàng OCB" value="Ngân hàng OCB" />
                <Picker.Item label="Ngân hàng VIBBank" value="Ngân hàng VIBBank" />
                <Picker.Item label="Ngân hàng SCB" value="Ngân hàng SCB" />
                <Picker.Item label="Ngân hàng Sacombank" value="Ngân hàng Sacombank" />
                <Picker.Item label="Ngân hàng BIDV" value="Ngân hàng BIDV" />
              </Picker>
              <Picker
                style={styles.textInput6}
                selectedValue={this.state.chucvu}
                onValueChange={(itemValue, itemIndex) => {
                  this.setState({ chucvu: itemValue }),
                    this.changeChucVu()
                }
                }
              >
                <Picker.Item label="Giám đốc" value="GD" />
                <Picker.Item label="Chủ hộ" value="CH" />
              </Picker>
              <TextInput
                style={[styles.textInput7, { borderColor: this.state.dientich_border }]}
                onChangeText={(text) => { this.setState({ dientich: text }), this.setState({ dientich_border: "gray" }) }}
              />
              <Picker
                style={styles.textInput8}
                selectedValue={this.state.gioitinh}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ gioitinh: itemValue })
                }
              >
                <Picker.Item label="Nam" value="nam" />
                <Picker.Item label="Nữ" value="nu" />
              </Picker>
              <TextInput
                style={[styles.textInput9, { borderColor: this.state.quoctich_border }]}
                placeholder="Việt Nam"
                onChangeText={(text) => { this.setState({ quoctich: text }), this.setState({ quoctich_border: "gray" }) }}
              />
              <TextInput
                style={[styles.textInput10, { borderColor: this.state.dantoc_border }]}
                placeholder="Kinh"
                onChangeText={(text) => { this.setState({ dantoc: text }), this.setState({ dantoc_border: "gray" }) }}
              />
              <TextInput
                style={[styles.textInput11, { borderColor: this.state.sohopdong_border }]}
                placeholder="ABC"
                onChangeText={(text) => { this.setState({ sohopdong: text }), this.setState({ sohopdong_border: "gray" }) }}
              />
            </View>
          </View>
          <View style={(styles.container)}>
            <View
              style={(styles.back)}
            >
              <TouchableOpacity
                style={(styles.back_touch)}
                onPress={() => this.props.navigation.navigate("qrcode")}
              >
                <Text
                  style={(styles.back_touch_text)}
                >
                  Trở về
                              </Text>
              </TouchableOpacity>
            </View>
            <View
              style={(styles.ok)}
            >
              <TouchableOpacity
                style={(styles.back_touch)}
                onPress={() => { this.adddata(this.state.tenhtx) }}
              >
                <Text
                  style={(styles.ok_touch_text)}
                >
                  OK
                              </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flexDirection: "row",
  },
  container3: {
    backgroundColor: '#cbf4f4',
  },
  container1: {
    // flex: 1,
    width: 110,

  },
  container2: {
    // flex: 1,
    height: 475,
    width: 300,
  },
  textInput: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: '#F5F5F5',
    width: 200,
    height: 30,
    marginTop: 2,
    marginLeft: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1,
  },
  header: {
    height: 30,
    alignItems: "center",
    backgroundColor: "#74eded"
  },
  text: {
    fontSize: 20,
    color: "blue"

  },
  textInput1: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 20,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  loai_hinh: {
    left: 10,
    height: 20,
    width: 220,
    top: 10,
    borderWidth: 1,
    backgroundColor: '#cbf4f4',

  },
  ten_NDD: {
    marginLeft: 4,
  },
  textInput2: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 35,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInput3: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 50,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput4: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 65,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput5: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    width: 300,
    height: 30,
    top: 90,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput6: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#cbf4f4',
    width: 220,
    height: 30,
    top: 105,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput7: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 120,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput8: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#cbf4f4',
    width: 220,
    height: 30,
    top: 135,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput9: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 150,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput10: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 165,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  textInput11: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#F5F5F5',
    width: 220,
    height: 30,
    top: 175,
    left: 15,
    textAlignVertical: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1
  },
  back: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: 'blue',
    width: 100,
    height: 40,
    marginTop: 65,
    marginLeft: 45,
    textAlign: 'center',
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1,
  },
  ok: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: 'blue',
    width: 100,
    height: 40,
    marginTop: 65,
    marginLeft: 70,
    padding: 2,
    borderColor: 'gray',
    borderWidth: 1,
  },
  back_touch: {
    width: 100,
    height: 40,
  },
  back_touch_text: {
    marginTop: 5,
    marginLeft: 20,
    color: "white"
  },
  ok_touch_text: {
    marginTop: 6,
    marginLeft: 30,
    color: "white"
  },

});