/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    style,
    Button,
    height,
    width,
    FlatList,
    Picker,
    TextInput,
    TouchableOpacity
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import { ScrollView } from 'react-native-gesture-handler';
export default class nhapthongtinchitietkhachhang extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date: "",
            date1: "",
            hoten: "",
            ngaysinh: "",
            diachivp: "",
            noio: "",
            sdt: "",
            email: "",
            zalo: "",
            cmnd: "",
            ngaycap: "",
            noicap: "",
            hoten_border: "gray",
            ngaysinh_border: "gray",
            diachivp_border: "gray",
            noio_border: "gray",
            sdt_border: "gray",
            email_border: "gray",
            zalo_border: "gray",
            cmnd_border: "gray",
            ngaycap_border: "gray",
            noicap_border: "gray",
            trangthai: "0",
            thongtinchitiet: {
                hoten: "",
                ngaysinh: "",
                diachivp: "",
                noio: "",
                sdt: "",
                email: "",
                zalo: "",
                cmnd: "",
                ngaycap: "",
                noicap: "",
            },
        }
    }
    componentDidMount() {
        const thongtinchitiet = this.props.navigation.getParam("thongtin", "");
        this.setState({ thongtinchitiet: thongtinchitiet })
        this.setState({
            hoten: thongtinchitiet.hoten,
            ngaysinh: thongtinchitiet.ngaysinh,
            diachivp: thongtinchitiet.diachivp,
            noio: thongtinchitiet.noio,
            sdt: thongtinchitiet.sdt,
            email: thongtinchitiet.email,
            zalo: thongtinchitiet.zalo,
            cmnd: thongtinchitiet.cmnd,
            ngaycap: thongtinchitiet.ngaycap,
            noicap: thongtinchitiet.noicap,
        })
        if (thongtinchitiet.ngaysinh != null) {
            this.setState({ date: thongtinchitiet.ngaysinh })
            this.setState({ date1: thongtinchitiet.ngaycap })
        } else {
            let date = new Date().getDate();
            let month = new Date().getMonth() + 1; //Current Month
            let year = new Date().getFullYear(); //Current Year
            this.setState({ date: year + '-' + month + '-' + date, date1: year + '-' + month + '-' + date })
        }
    }

    backbutton() {
        var trangthai = 0;
        if (this.state.hoten == null) {
            this.setState({ hoten_border: "red" });
            trangthai = 1;
        }
        if (this.state.ngaysinh == null) {
            this.setState({ ngaysinh_border: "red" });
            trangthai = 1;
        }
        var thongtinchitiet = this.props.navigation.getParam("loaihinh", "");
        if (this.state.diachivp == null) {
            if(thongtinchitiet =="DN"){
                this.setState({ diachivp_border: "red" });
                trangthai = 1;
            }
        }
        if (this.state.noio == null) {
            this.setState({ noio_border: "red" });
            trangthai = 1;
        }
        if (this.state.sdt == null) {
            this.setState({ sdt_border: "red" });
            trangthai = 1;
        }
        if (this.state.email == null) {
            this.setState({ email_border: "red" });
            trangthai = 1;
        }
        if (this.state.zalo == null) {
            this.setState({ zalo_border: "red" });
            trangthai = 1;
        }
        if (this.state.cmnd == null) {
            this.setState({ cmnd_border: "red" });
            trangthai = 1;
        }
        if (this.state.ngaycap == null) {
            this.setState({ ngaycap_border: "red" });
            trangthai = 1;
        }
        if (this.state.noicap == null) {
            this.setState({ noicap_border: "red" });
            trangthai = 1;
        }
        // trangthai = 0;
        if (trangthai == 1) {
            return;
        }

        this.setState({
            thongtinchitiet: {
                hoten: this.state.hoten,
                ngaysinh: this.state.ngaysinh,
                diachivp: this.state.diachivp,
                noio: this.state.noio,
                sdt: this.state.sdt,
                email: this.state.email,
                zalo: this.state.zalo,
                cmnd: this.state.cmnd,
                ngaycap: this.state.ngaycap,
                noicap: this.state.noicap,
            }
        })
        setTimeout(() => {
            this.props.navigation.navigate('nhapthongtinkhachhang', { thongtin: this.state.thongtinchitiet })
        }, 1);
    }
    handleDateChange = (date) => {
        this.setState({ date })
    }
    handleDateChange1 = (date1) => {
        this.setState({ date1 });
    }

    render() {
        var thongtinchitiet = this.props.navigation.getParam("thongtin", "");
        return (
            <ScrollView style={styles.container3}>
                <View style={styles.container3}>
                    <View style={styles.header}>
                        <Text style={styles.text}>
                            Nhập Thông Tin Cá Nhân
                    </Text>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.container1}>
                            <Text style={{ top: 10, left: 5 }}>
                                Họ và tên:
                        </Text>
                            <Text style={{ top: 30, left: 5 }}>
                                Năm sinh:
                        </Text>
                            <Text style={{ top: 45, left: 5 }}>
                                Địa chỉ văn phòng:
                        </Text>
                            <Text style={{ top: 60, left: 5 }}>
                                Địa chỉ nơi ở:
                        </Text>
                            <Text style={{ top: 85, left: 5 }}>
                                Số điện thoại:
                        </Text>
                            <Text style={{ top: 120, left: 5 }}>
                                Email:
                        </Text>
                            <Text style={{ top: 150, left: 5 }}>
                                Zalo:
                        </Text>
                            <Text style={{ top: 175, left: 5 }}>
                                CMND:
                        </Text>
                            <Text style={{ top: 200, left: 5 }}>
                                Ngày cấp:
                        </Text>
                            <Text style={{ top: 220, left: 5 }}>
                                Nơi cấp:
                        </Text>


                        </View>
                        <View style={styles.container2}>
                            <View>
                                <TextInput
                                    style={[styles.textInput0, { borderColor: this.state.hoten_border }]}
                                    value={this.state.hoten}
                                    placeholder="họ tên"
                                    onChangeText={(text) => { this.setState({ hoten: text }), this.setState({ hoten_border: "gray" }) }}
                                />
                                <View style={(styles.date)}>
                                    <DatePicker
                                        color="#296b3e"
                                        date={this.state.date}
                                        errorColor="#c32c27"
                                        handleDateChange={this.handleDateChange}
                                        androidMode="spinner"
                                        hoverWeek
                                        customStyles={{
                                            dateInput: {
                                                borderColor: this.state.ngaysinh_border
                                            }
                                        }}

                                        lightHeader
                                        onDateChange={(date) => { this.setState({ date: date }), this.setState({ ngaysinh: date }), this.setState({ ngaysinh_border: "gray" }) }}
                                    />
                                </View>
                                <TextInput
                                    style={[styles.textInput2, { borderColor: this.state.diachivp_border }]}
                                    placeholder="địa chỉ văn phòng"
                                    value={this.state.diachivp}
                                    onChangeText={(text) => { this.setState({ diachivp: text }), this.setState({ diachivp_border: "gray" }) }}
                                />
                                <TextInput
                                    style={[styles.textInput3, { borderColor: this.state.noio_border }]}
                                    value={this.state.noio}
                                    onChangeText={(text) => { this.setState({ noio: text }), this.setState({ noio_border: "gray" }) }}
                                    placeholder="nơi ở"
                                />
                                <TextInput
                                    style={[styles.textInput4, { borderColor: this.state.sdt_border }]}
                                    value={this.state.sdt}
                                    onChangeText={(text) => { this.setState({ sdt: text }), this.setState({ sdt_border: "gray" }) }}
                                    placeholder="số điện thoai"
                                    keyboardType='numeric'
                                    maxLength={10}
                                />
                                <TextInput
                                    onChangeText={(text) => { this.setState({ email: text }), this.setState({ email_border: "gray" }) }}
                                    style={[styles.textInput5, { borderColor: this.state.email_border }]}
                                    value={this.state.email}
                                    placeholder="abc@gmail.com"
                                />
                                <TextInput
                                    onChangeText={(text) => { this.setState({ zalo: text }), this.setState({ zalo_border: "gray" }) }}
                                    style={[styles.textInput6, { borderColor: this.state.zalo_border }]}
                                    value={this.state.zalo}
                                    placeholder="sđt zalo"
                                    keyboardType='numeric'
                                    maxLength={10}
                                />
                                <TextInput
                                    onChangeText={(text) => { this.setState({ cmnd: text }), this.setState({ cmnd_border: "gray" }) }}
                                    style={[styles.textInput7, { borderColor: this.state.cmnd_border }]}
                                    value={this.state.cmnd}
                                    placeholder="CMND"
                                    keyboardType='numeric'
                                    maxLength={9}
                                />
                                <View style={(styles.date1)}>
                                    <DatePicker
                                        color="#296b3e"
                                        date={this.state.date1}
                                        errorColor="#c32c27"
                                        handleDateChange={this.handleDateChange1}
                                        androidMode="spinner"
                                        hoverWeek
                                        customStyles={{
                                            dateInput: {
                                                borderColor: this.state.ngaycap_border
                                            }
                                        }}
                                        lightHeader
                                        onDateChange={(date) => { this.setState({ date1: date }), this.setState({ ngaycap: date }), this.setState({ ngaycap_border: "gray" }) }}
                                    />
                                </View>
                                <TextInput
                                    onChangeText={(text) => { this.setState({ noicap: text }), this.setState({ noicap_border: "gray" }) }}
                                    style={[styles.textInput9, { borderColor: this.state.noicap_border }]}
                                    value={this.state.noicap}
                                    placeholder="CA.Cần Thơ"
                                />
                            </View>
                        </View>
                    </View>
                    <View style={(styles.container)}>
                        <View
                            style={(styles.back)}
                        >
                            <TouchableOpacity
                                style={(styles.back_touch)}
                                onPress={() => {this.props.navigation.navigate('nhapthongtinkhachhang', { thongtin: this.state.thongtinchitiet })}}
                            >
                                <Text
                                    style={(styles.back_touch_text)}
                                >
                                    Trở về
                            </Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={(styles.ok)}
                        >
                            <TouchableOpacity
                                style={(styles.back_touch)}
                                onPress={() => { this.backbutton() }}
                            >
                                <Text
                                    style={(styles.ok_touch_text)}
                                >
                                    OK
                            </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flexDirection: "row",
    },
    bordercolor: {
        borderColor: "red",
    },
    date: {
        flexDirection: "row",
        top: 20,
        left: 15,
    },
    date1: {
        flexDirection: "row",
        top: 135,
        left: 15,
    },
    container3: {
        backgroundColor: '#cbf4f4',
    },
    container1: {
        // flex: 1,
        width: 110,
        height: height,
    },
    container2: {
        // flex: 1,
        width: 300,
        height: 400,
    },
    back: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 100,
        height: 40,
        marginTop: 50,
        marginLeft: 45,
        textAlign: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    ok: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 100,
        height: 40,
        marginTop: 50,
        marginLeft: 70,
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    back_touch: {
        width: 100,
        height: 40,
    },
    back_touch_text: {
        marginTop: 5,
        marginLeft: 20

    },
    ok_touch_text: {
        marginTop: 6,
        marginLeft: 30,

    },
    textInput: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 200,
        height: 30,
        marginTop: 2,
        marginLeft: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    header: {
        height: 30,
        alignItems: "center",
        backgroundColor:"#74eded"
    },
    text: {
        fontSize: 20,
        color: "blue"

    },
    ngay: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        width: 90,
        height: 30,
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor: 'white',
    },
    thang: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        width: 100,
        height: 30,
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor: 'white',
    },
    textInput0: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 10,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    loai_hinh: {
        left: 10,
        height: 20,
        width: 300,
        top: 10,
        borderWidth: 1
    },
    textInput2: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 35,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput3: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 50,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput4: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 65,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput5: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 90,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput6: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 105,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput7: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 120,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput8: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 135,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput9: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 150,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput10: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 165,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },
    textInput11: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#F5F5F5',
        width: 220,
        height: 30,
        top: 175,
        left: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1
    },


});