/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    style,
    Button,
    height,
    width,
    FlatList,
    Picker,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    BackHandler, Alert, ToastAndroid,
} from 'react-native';
export default class dangnhap extends React.Component {
    _didFocusSubscription;
    _willBlurSubscription;
    constructor(props) {
        super(props);
        this._didFocusSubscription = props.navigation.addListener(
            'didFocus',
            payload =>
                BackHandler.addEventListener(
                    'hardwareBackPress',
                    this.onBackButtonPressAndroid
                )
        );
        this.state = {
            tk: "",
            mk: ""
        }
    }

    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener(
            'willBlur',
            payload =>
                BackHandler.removeEventListener(
                    'hardwareBackPress',
                    this.onBackButtonPressAndroid
                )
        );
    }

    onBackButtonPressAndroid = () => {
        Alert.alert(
            'Thông báo',
            'Bạn có muốn thoát không?',
            [
                {
                    text: 'Cancel',
                    onPress: () => { return true; },
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        )
        return true;
    };

    componentWillUnmount() {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }
    dangnhap(tk,mk){
        return fetch('https://vothanhbach1997.000webhostapp.com/dangnhap.php', {
        method: "POST",
        body: JSON.stringify({
          taikhoan: this.state.tk,
          matkhau: this.state.mk,
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function () {

          });
          data = [this.state.dataSource]
          data.map((data, index) => {
              if(data.text == "true"){
                  this.props.navigation.navigate("nhapthongtinkhachhang");
              }
              else {
                Alert.alert(
                        'Thông báo',
                        'Tài khoản hoặc mặt khẩu không đúng',
                        [
                            {
                                text: 'ok',
                                onPress: () => { return true; },
                                style: 'default',
                            },
                        ],
                        { cancelable: false },
                    )
              }
            key = { index }
          })
        })
        .catch((error) => {
        //   Alert.alert(
        //     'Thông báo',
        //     'Có 1 vài thông tin không đúng',
        //     [
        //         {
        //             text: 'ok',
        //             onPress: () => { return true; },
        //             style: 'default',
        //         },
        //     ],
        //     { cancelable: false },
        // )
        });
    }
    render() {
        return (
            <ImageBackground
                source={require("./image/cam.jpg")} style={{ width: '100%', height: '100%', }}
                resizeMode='cover'
            >
                <View
                    style={styles.container}
                >
                    <Text
                        style={styles.text}
                    >Tài khoản</Text>
                    <Text
                        style={styles.text1}
                    >Mật khẩu</Text>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(text) => this.setState({ tk: text })}
                        ></TextInput>
                    </View>
                    <View>
                        <TextInput
                            style={styles.textInput1}
                            onChangeText={(text) => this.setState({ mk: text })}
                            secureTextEntry={true}
                        ></TextInput>
                    </View>
                    <View
                        style={(styles.ok)}
                    >
                        <TouchableOpacity
                            style={(styles.back_touch)}
                            onPress={() => this.dangnhap(this.state.tk, this.state.mk)}
                        >
                            <Text
                                style={(styles.ok_touch_text)}
                            >
                                Đăng Nhập
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        top: 100,
        left: 5,
    },
    container3: {
        flexDirection: "column",
        backgroundColor: 'white',
    },
    text: {
        top: 131,
        left: 20,
        color: "white",
        fontSize: 20,
    },
    text1: {
        top: 148,
        left: 20,
        color: "white",
        fontSize: 20,
    },
    container2: {
        // flex: 1,
        height: 475,
        width: 300,
    },
    textInput: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 200,
        height: 30,
        marginTop: 80,
        marginLeft: 120,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    textInput1: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 200,
        height: 30,
        marginTop: 10,
        marginLeft: 120,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    ok: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#FF8C00',
        width: 200,
        height: 40,
        marginTop: 25,
        marginLeft: 90,
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    back_touch: {
        width: 200,
        height: 40,
    },
    back_touch_text: {
        marginTop: 15,
        marginLeft: 20,
        color: "white"
    },
    ok_touch_text: {
        marginTop: 3,
        marginLeft: 45,
        fontSize: 20
    },

});