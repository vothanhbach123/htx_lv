import React from 'react';
import {
    FlatList, ActivityIndicator, Text, View, TextInput, StyleSheet,
    Button, TouchableOpacity, Picker
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
// import { TouchableOpacity } from 'react-native-gesture-handler';
export default class danhsachkhachhang extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            text: '',
            tong: 0,
            doanhnghiep: 0,
            tapthe: 0,
            canhan: 0,
            loaihinh: 'tatca',
            tinh: 'tatca',
            loaikhachhang: 'tatca',
        }
    }

    async bach(text) {
        return fetch('https://vothanhbach1997.000webhostapp.com/dskhachhang.php', { method: "POST", body: JSON.stringify({ 
                    loaihinh: this.state.loaihinh, tinh: this.state.tinh, loaikhachhang: this.state.loaikhachhang}) })
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    dataSource: responseJson,
                }, function () {
                    data = this.state.dataSource;
                    var key = 0;
                    var temp = "";
                    var canhan = 0;
                    var tapthe = 0;
                    var doanhnghiep = 0;
                    data.map((data, index) => {
                        temp = data.MAKH
                        if(temp.slice(0,2) == "CN"){
                            canhan++
                            this.setState({canhan:canhan})
                        }
                        if(temp.slice(0,2) == "TT"){
                            tapthe++
                            this.setState({tapthe:tapthe})
                        }
                        if(temp.slice(0,2) == "DN"){
                            doanhnghiep++
                            this.setState({doanhnghiep:doanhnghiep})
                        }
                        key++
                    })
                    
                    this.setState({tong:key})
                });

            })
            .catch((error) => {
                alert("Tìm không thấy dữ liệu!");
            });
    }
    click(item) {
        alert(item.MAKH)
    }
    render() {
        return (
            <View style={styles.container}>
                <View
                    style={styles.view_fill}
                >
                    <TextInput
                        style={styles.textInput}
                        placeholder="Mã khác hàng"
                    ></TextInput>
                    <TouchableOpacity
                        onPress={() => this.bach(this.state.text)}
                        style={styles.button}
                    >
                        <Text
                            style={styles.text_timkiem}
                        > Tìm kiếm</Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={styles.view_fill}
                >
                    <View
                        style={styles.view_fill_picker}
                    >
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.loaihinh}
                            onValueChange={(itemValue, itemIndex) => { this.setState({ loaihinh: itemValue })}}           
                        >
                            <Picker.Item label="Tất cả" value="tatca" />
                            <Picker.Item label="Tập thể" value="TT" />
                            <Picker.Item label="Doanh nghiệp" value="DN" />
                            <Picker.Item label="Cá nhân" value="CN" />
                        </Picker>
                    </View>
                    <View
                        style={styles.view_fill_picker}
                    >
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.tinh}
                            onValueChange={(itemValue, itemIndex) => { this.setState({ tinh: itemValue })}}            
                        >
                            <Picker.Item label="Tất cả" value="tatca" />
                            <Picker.Item label="Cần Thơ" value="cantho" />
                            <Picker.Item label="Vĩnh Long" value="vinhlong" />
                            <Picker.Item label="Tiền Giang" value="tiengiang" />
                            <Picker.Item label="Hậu Giang" value="haugiang" />
                        </Picker>
                    </View>
                    <View
                        style={styles.view_fill_picker3}
                    >
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.loaikhachhang}
                            onValueChange={(itemValue, itemIndex) => { this.setState({ loaikhachhang: itemValue }) }}            
                        >
                            <Picker.Item label="Tất cả" value="tatca" />
                            <Picker.Item label="Bán nông sản" value="bns" />
                            <Picker.Item label="Mua vật tư" value="mvt" />
                        </Picker>
                    </View>
                </View>
                <View style={({backgroundColor: '#cbf4f4'})}>
                <View
                    style={[styles.view_fill4,{backgroundColor: '#cbf4f4'}]}
                >
                    <Text style = {{fontSize:13, marginLeft: 120}}>
                        Tập thể
                    </Text>
                    <Text style = {{fontSize:13, marginLeft: 30}}>
                        Doanh nghiệp
                    </Text>
                    <Text style = {{fontSize:13, marginLeft: 30}}>
                        Cá nhân
                    </Text>
                </View>
                <View
                    style={[styles.view_fill4,{backgroundColor: '#cbf4f4'}]}
                >
                    <Text style = {{width:100}}>Tổng: {this.state.tong}</Text>
                    <Text style = {{width:35,fontSize:13, marginLeft: 35}}>{this.state.doanhnghiep}</Text>
                    <Text style = {{width:60,fontSize:13, marginLeft: 55}}>{this.state.tapthe}</Text>
                    <Text style = {{width:100,fontSize:13, marginLeft: 30}}>{this.state.canhan}</Text>
                </View>
                </View>
                <View style={{ flex: 1, paddingTop: 15 }}>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({ item }) =>
                            <View
                                style={styles.View}
                            >
                                <Text
                                    onPress={this.click.bind(this, item)}
                                    style={{ width: 100, marginLeft: 10 }}
                                >
                                    {item.NDD}
                                </Text>
                                <Text
                                    style={{ marginLeft: 5 }}
                                >bachdeptrai</Text>
                            </View>
                        }
                        keyExtractor={({ MAKH }, index) => MAKH}
                    />
                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    View: {
        borderBottomWidth: 1,
        borderColor: "gray",
        marginTop: 20,
        flexDirection: "row",
    },
    view_fill: {
        flexDirection: "row",
        backgroundColor:"#74eded"
    },
    view_fill4: {
        flexDirection: "row",
        marginTop:5,
    },
    view_fill_picker: {
        height: 30,
        width: 120,
        borderEndWidth: 1,
        marginTop: 5,
        borderColor:"gray"
    },
    picker: {
        marginLeft: 5,
        width: 180,
        height: 30,
        backgroundColor: "transparent"
    },
    view_fill_picker3: {
        height: 30,
        width: 126,
        borderEndWidth: 1,
        marginTop: 5,
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        height: "100%",
        width: "100%",
        justifyContent: 'flex-end',
    },
    button: {
        width: 100,
        height: 30,
        backgroundColor: "blue",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginTop: 15,
        marginLeft: 15,
    },
    textInput: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F5F5F5',
        width: 200,
        height: 30,
        marginTop: 15,
        marginLeft: 15,
        textAlignVertical: 'center',
        padding: 2,
        borderColor: 'gray',
        borderWidth: 1,
    },
    text_timkiem: {
        marginTop: 5,
        marginLeft: 15,
        color: "white"
    }
});
