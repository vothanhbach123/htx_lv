/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation'
import nhapthongtinchitietkhachhang from './nhapthongtinchitietkhachhang';
import nhapthongtinkhachhang from './nhapthontinkhachhang';
import dangnhap from './dangnhap';
import flash from './flash';
import danhsachkhachhang from './danhsachkhachhang';
import qrcode from './qrcode';

const navigator = createStackNavigator(
  {
    flash: {
      screen: flash,
      navigationOptions: () => ({
        header: null
      }),
    },
    dangnhap: {
      screen: dangnhap,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    danhsachkhachhang: {
      screen: danhsachkhachhang,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    nhapthongtinkhachhang: {
      screen: nhapthongtinkhachhang,
      navigationOptions: () => ({
        header: null
      }),
    },
    nhapthongtinchitietkhachhang: {
      screen: nhapthongtinchitietkhachhang,
      navigationOptions: () => ({
        header: null
      }),
    },
    qrcode: {
      screen: qrcode,
      navigationOptions: () => ({
        header: null
      }),
    },
  },
  {initialRouteName:"nhapthongtinkhachhang"}
);
const AppNavigator = createAppContainer(navigator);
export default AppNavigator; 

