/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    style,
    Button,
    height,
    width,
    FlatList,
    Picker,
    TextInput,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
export default class flash extends React.Component {
    next(){
        setTimeout(() => {
            this.props.navigation.navigate("dangnhap")
        }, 5000);
    }

    render() {
        this.next()
        return (
            <ImageBackground source={require("./image/cam2.jpg")} style={{ width: '100%', height: '100%' }}>
                <View
                style = {styles.container}
                >
                <View>
                <Text
                style = {[styles.text, {top:100, fontWeight: 'bold', textShadowColor:'#065106',textShadowOffset: {
                    width: 1,
                    height: 1,
                },
                textShadowRadius: 5.00,
                textDecorationColor:"#ff0000"
                }]}
                >Chào mừng bạn</Text>
                </View>
                <View>
                <Text
                style = {[styles.text, {top:100, fontWeight: 'bold', textShadowColor:'#065106',textShadowOffset: {
                    width: 1,
                    height: 1,
                },
                textShadowRadius: 5.00,
                textDecorationColor:"#ff0000"
                }]}                
                >đến với hợp tác xã nông nghiệp</Text>
                </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        top: 100,
        left: 5,
    },
    text:{
        fontSize: 35,
        color: "#00ffff",
        textAlign: "center"
    }

});